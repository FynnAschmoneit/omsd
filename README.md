# OMSD
The Open Membrane System Design Tool. A model-based design tool for the simulation of enitre membrane filtration plants, using Matlab/Simulink.

Details and reference: Aschmoneit, F., Hélix-Nielsen, C. OMSD - An open membrane system design tool, Separation and Purification Technology, 2020, Vol. 233, doi:10.1016/j.seppur.2019.115975
